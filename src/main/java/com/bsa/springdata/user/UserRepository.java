package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    @Query(value = "select u from User u where u.office.city = :city order by u.lastName asc")
    List<User> findByCity(@Param("city") String city);

    List<User> findByExperienceGreaterThanEqual(int experience);

    @Query("select u from User u where u.office.city = :city and u.team.room = :room")
    List<User> findByRoomAndCity(
            @Param("city") String city,
            @Param("room") String room, Sort seatNumber);

    @Modifying
    @Query("delete from User u where u.experience < :experience ")
    int deleteByExperience(@Param("experience") int experience);
}
