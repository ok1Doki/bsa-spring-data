package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("select o from Office o " +
            "join o.users u " +
            "join u.team t " +
            "join t.technology te " +
            "where te.name = :technology " +
            "group by o.id")
    List<Office> findByTechnology(@Param("technology") String technology);

    @Query("update Office o set o.address = :newAddress where o.address = :oldAddress and o.id in ( " +
            "select o1.id from Office o1 " +
            "join o1.users u " +
            "join u.team t " +
            "where t.project is not null)")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    Integer updateAddress(
            @Param("oldAddress") String oldAddress,
            @Param("newAddress") String newAddress);

    Optional<Office> findByAddress(String address);
}
