package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TechnologyRepository technologyRepository;

    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository
                .findByTechnology(technology)
                .stream()
                .sorted((o1, o2) -> Integer.compare(
                        o1.getTeams().stream().mapToInt(team -> team.getUsers().size()).sum(),
                        o2.getTeams().stream().mapToInt(team -> team.getUsers().size()).sum()))
                .limit(5)
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<Project> findTheBiggest() {
        return Optional.of(projectRepository.findTheBiggest().get(0));
    }

    public List<ProjectSummaryDto> getSummary() {
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
//        return projectRepository.getSummary();
        return Collections.emptyList();
    }

    public int getCountWithRole(String role) {
        return projectRepository.countWithRole(role).stream().map(Project::getId).collect(Collectors.toSet()).size();
    }

    @Transactional
    public UUID createWithTeamAndTechnology(CreateProjectRequestDto requestDto) {
        Technology technology = Technology
                .builder()
                .name(requestDto.getTech())
                .description(requestDto.getTechDescription())
                .link(requestDto.getTechLink())
                .build();
        technology = technologyRepository.save(technology);

        Team team = Team
                .builder()
                .area(requestDto.getTeamArea())
                .room(requestDto.getTeamRoom())
                .name(requestDto.getTeamName())
                .technology(technologyRepository.getOne(technology.getId()))
                .build();
        teamRepository.save(team);

        Project project = Project
                .builder()
                .name(requestDto.getProjectName())
                .description(requestDto.getProjectDescription())
                .teams(Collections.singletonList(teamRepository.getOne(team.getId())))
                .build();

        projectRepository.save(project);

        return project.getId();
    }
}