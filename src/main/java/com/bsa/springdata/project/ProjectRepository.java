package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query(value =
            "select p from Team t " +
                    "join t.project p " +
                    "where t.technology.name = :technology")
    List<Project> findByTechnology(@Param("technology") String technology);

    @Query(value = "select p from Team t " +
            "join t.project p " +
            "where p.teams.size = (select max(p1.teams.size) from Project p1) " +
            "and t.users.size = (select max(t1.users.size) from Team t1)" +
            "order by p.name desc ")
    List<Project> findTheBiggest();

    //TODO: rewrite this query
    @Query("select p from Project p " +
            "left join p.teams t " +
            "left join t.users u " +
            "join u.roles r " +
            "where r.name = :role ")
    List<Project> countWithRole(@Param("role") String role);


//      Вибірка загальної інформації по проектах
//      Вибрати наступні дані про проект: назва, кількість команд, кількість розробників, список технологій (через кому).
//      Сортувати дані по імені проекту по зростанню.
//      Оскільки перетворити значення технологій в список, розділений комами, досить складно стандартними засобами JPQL,
//      можна використовувати nativeQuery і проекцію даних на інтерфейс ProjectSummaryDto.
//      Якщо побудувати запит таким чином не вийде, то можна використовувати будь-який інший спосіб, який відповідає тестам.

//    @Query(value = "SELECT new com.bsa.springdata.project.dto.ProjectSummaryDto(" +
//            "p.name, " +
//            "p.teams.size, " +
//            "FUNCTION('string_agg', FUNCTION('to_char',r.id, '999999999999'), ',')) "
//            + "FROM Technology te, Team t, Project p "
//            + "WHERE ac.status = 'Failed' "
//            + "AND ac.checkpoint = c.id "
//            + "AND r.id = c.testResult "
//            + "AND r.testRunExecutionLogId = :executionId "
//            + "GROUP by r.testScriptName, r.testScriptVersion, c.name, ac.name, ac.errorMessage "
//            + "ORDER by ec desc")
//    List<ProjectSummaryDto> getSummary();
}