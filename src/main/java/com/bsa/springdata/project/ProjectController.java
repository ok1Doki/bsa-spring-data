package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.user.UserService;
import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/top5")
    public List<ProjectDto> getUser(@RequestParam String technology) {
        return projectService.findTop5ByTechnology(technology);
    }

    @GetMapping("/biggest")
    public Optional<ProjectDto> findBiggestProject(){
        return projectService.findTheBiggest().map(ProjectDto::fromEntity);
    }

    @GetMapping("/summary")
    public List<ProjectSummaryDto> getSummary(){
        return projectService.getSummary();
    }
}
