package com.bsa.springdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

@SpringBootApplication
@EnableJdbcRepositories
public class SpringDataApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringDataApplication.class, args);
	}
}
